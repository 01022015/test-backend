<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\Buy\Read;
use App\Services\Buy\GenerateCsv;
use App\Services\Buy\ExceptionEmptyData;
use App\Services\Buy\CsvWriter;

class BuyGenerateCsvTest extends TestCase
{
    /**
     * @test
     */
    public function sholdeBeExceptionEmptyWhenExecute(){

        $buyRead = $this->createMock(Read::class);
        $csvWriter = $this->createMock(CsvWriter::class);

        $buyGenerateCsv = new GenerateCsv($buyRead,$csvWriter);
        $buyRead->method("execute")->willReturn([]);

        $this->expectException(ExceptionEmptyData::class);

        $buyGenerateCsv->execute();

    }

    /**
     * @test
     */
    public function sholdeBeCreateCsvWhenExecute(){

        $buyRead = $this->createMock(Read::class);
        $csvWriter = $this->createMock(CsvWriter::class);

        $buyGenerateCsv = new GenerateCsv($buyRead,$csvWriter);
        $buyRead->method("execute")->willReturn([
            [
                'mes' => 'Janeiro',
                'categoria' => 'Alimentos',
                'produto' => 'Creme dental',
                'quantidade' => 100
            ]
        ]);

        $csvWriter->method('save')->willReturn(true);

        $generate = $buyGenerateCsv->execute();

        $this->assertTrue($generate);

    }
}
