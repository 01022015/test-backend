<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Category;
use App\Product;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\BuyRepository;
use App\Services\Buy\Read;
use App\Services\Buy\ExceptionEmptyData;
use App\Services\Buy\Seed;

class BuySeedTest extends TestCase
{
    /**
     * @test
     */
    public function sholdeBeExceptionEmptyWhenExecute(){

        $buyRead = $this->createMock(Read::class);
        $buyRepository = $this->createMock(BuyRepository::class);
        $categoryRespository = $this->createMock(CategoryRepository::class);
        $productRepository = $this->createMock(ProductRepository::class);

        $buyGenerateCsv = new Seed($buyRead,$buyRepository,$categoryRespository,$productRepository);
        $buyRead->method("execute")->willReturn([]);

        $this->expectException(ExceptionEmptyData::class);

        $buyGenerateCsv->execute();

    }

    /**
     * @test
     */
    public function sholdeBeSaveBuyWhenExecute(){

        $buyRead = $this->createMock(Read::class);
        $buyRepository = $this->createMock(BuyRepository::class);
        $categoryRespository = $this->createMock(CategoryRepository::class);
        $productRepository = $this->createMock(ProductRepository::class);

        $buyGenerateCsv = new Seed($buyRead,$buyRepository,$categoryRespository,$productRepository);
        $buyRead->method("execute")->willReturn([
            [
                'mes' => 'Janeiro',
                'categoria' => 'Alimentos',
                'produto' => 'Creme dental',
                'quantidade' => 100
            ]
        ]);

        $category = new Category();
        $category->id = 1;

        $product = new Product();
        $product->id = 1;

        $categoryRespository->method('saveOrUpdate')->willReturn($category);
        $productRepository->method('saveOrUpdate')->willReturn($product);
        $buyRepository->method('save')->willReturn(true);

        $seed = $buyGenerateCsv->execute();

        $this->assertTrue($seed);

    }

}
