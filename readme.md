# Teste backend

Escolhi utilizar o framework `laravel`, devido aos ótimos recursos que ele oferece.

## Configurações

Crie um arquivo `.env` e preencha com os dados do seu banco de dados:

```
APP_NAME=AkanaTest
APP_ENV=local
DB_CONNECTION=mysql
DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=akana
DB_USERNAME=
DB_PASSWORD=
```

### Instale as dependências:

```php
composer i
```
### Rode as migrations
```php 
php artisan migrate
``` 

# O Desafio

O arquivo de estrutura se encontra em `storage/app/buy/read/lista-de-compras.php`

O arquivo `SQL` encontra-se em `database/teste.sql`

## Parte 1: 

Execute a Linha de comando abaixo para gerar o csv:
```php 
php artisan buy:csv lista-de-compras
```

O arquivo será criado no diretório `storage/app/buy/csv/lista-de-compras.csv`


## Parte 2: 

Execute a linha de comando abaixo para subir as compras no banco de dados:
```php 
php artisan buy:seed lista-de-compras
```

# Testes unitários
Criei alguns testes unitários:

Para rodar os testes execute:
```
vendor/bin/phpunit tests/
```
