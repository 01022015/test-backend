<?php

namespace App\Console\Commands;

use App\Services\Buy\CorrectSpelling;
use App\Services\Buy\CsvWriter;
use App\Services\Buy\GenerateCsv as BuyGenerateCsv;
use App\Services\Buy\Read;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class GenerateCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'buy:csv {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload de csv das compras';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Gerando csv de vendas!");

        $correctSpelling = new CorrectSpelling;
        $read = new Read(
            Storage::path("buy/read/{$this->argument('filename')}.php"),
            $correctSpelling
        );

        $csvWriter = new CsvWriter;

        $generateCsv = new BuyGenerateCsv($read,$csvWriter);

        if($generateCsv->execute()){
            $this->info("Csv de vendas gerado!");
            $this->info(pathinfo($generateCsv->getPath())['basename']);
        } else {
            $this->error('Não foi possivel gerar o csv');
        }

    }
}
