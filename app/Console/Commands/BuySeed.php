<?php

namespace App\Console\Commands;

use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\BuyRepository;
use App\Services\Buy\CorrectSpelling;
use App\Services\Buy\Read;
use App\Services\Buy\Seed;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use DB;

class BuySeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'buy:seed {filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subir compras através de arquivo';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Iniciando seed de vendas!");

        $correctSpelling = new CorrectSpelling;
        $read = new Read(
            Storage::path("buy/read/{$this->argument('filename')}.php"),
            $correctSpelling
        );

        $buyRepository = new BuyRepository;
        $categoryRepository = new CategoryRepository;
        $productRepository = new ProductRepository;

        DB::beginTransaction();

        try {

            $seed = new Seed($read,$buyRepository,$categoryRepository,$productRepository);

            if($seed->execute()){
                $this->info("Seed de vendas completado");
            }else{
                throw new \Exception("Não foi possivel salvar as vendas");
            }

            DB::commit();

        } catch (\Exception $e) {
            DB::rollBack();
            $this->error($e->getMessage());
        }

    }
}
