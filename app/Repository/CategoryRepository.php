<?php 

namespace App\Repository;
use App\Category;

class CategoryRepository{

    public function saveOrUpdate(Category $category){
        return Category::updateOrCreate(
            ['name'=>$category->name],
            ['name'=>$category->name]
        );
    }

}