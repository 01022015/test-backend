<?php 

namespace App\Repository;
use App\Product;

class ProductRepository{

    public function saveOrUpdate(Product $product){
        return Product::updateOrCreate(
            ['name'=>$product->name,'category_id' => $product->category_id],
            ['name'=>$product->name,'category_id' => $product->category_id]
        );
    }

}