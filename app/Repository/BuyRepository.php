<?php

namespace App\Repository;

use App\Buy;

class BuyRepository {

    public function save(Buy $buy){
        return $buy->save();
    }

}