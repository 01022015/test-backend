<?php

namespace App\Services\Buy;

class CorrectSpelling{

    public $words = [
        "Papel Hignico" => "Papel Higiênico",
        "Brocolis" => "Brócolis",
        "Chocolate ao leit" => "Chocolate ao leite",
        "Sabao em po" => "Sabão em pó"
    ];

    public function execute($text){

        if(isset($this->words[trim($text)]))
            return $this->words[trim($text)];
        
        return $text;

    }

}