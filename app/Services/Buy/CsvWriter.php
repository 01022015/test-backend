<?php

namespace App\Services\Buy;

use Illuminate\Support\Facades\Storage;
use League\Csv\Writer;

class CsvWriter implements CsvWriterContract{

    public function save($header,$records,$path){
        
        $csv = Writer::createFromString();
        $csv->setDelimiter(';');

        $csv->insertOne($header);
        $csv->insertAll($records);
        
        return file_put_contents($path,$csv->getContent());

    }

}