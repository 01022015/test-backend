<?php

namespace App\Services\Buy;

class Read{

    private $path;
    private $correctSpelling;

    public function __construct($filename, CorrectSpelling $correctSpelling)
    {
        $this->path = $filename;
        $this->correctSpelling = $correctSpelling;
    }

    public function execute(){

        $aData = require_once($this->path);
        return $this->getData($aData);
        
    }

    private function getData($data){
        
        $buys = [];

        foreach($data as $mes => $buy){

            foreach($buy as $categoria => $produtos){
                
                foreach($produtos as $produto => $quantidade){
                    $buys[] = [
                        'mes' => $this->getMonthName($mes),
                        'categoria' => $this->getCategoryName($categoria),
                        'produto' => $this->correctSpelling($produto),
                        'quantidade' => (int)$quantidade
                    ];
                }

            }

        }

        return $this->order($buys);

    }

    public function order($buys){

        $buysOrdened = [];

        foreach($buys as $key => $buy){

            $tamanhoFake = 9999999;

            $chaves = [];
            $chaves[] = $this->getMonthKey($buy['mes']);
            $chaves[] = $this->getCategoriaKey($buy['categoria']);
            $chaves[] = $tamanhoFake - $buy['quantidade'];
            $chaves[] = $key;

            $chaveDeOrdenacao = implode('_',$chaves);
            $buysOrdened[$chaveDeOrdenacao] = $buy;
        }

        ksort($buysOrdened);

        return $buysOrdened;

    }

    public function getMonthName($mes){
        $meses = [
            'janeiro' => 'Janeiro',
            'fevereiro' => 'Fevereiro',
            'marco' => 'Março',
            'abril' => "Abril",
            "maio" => 'Maio',
            'junho' => "Junho",
            "julho" => 'Julho',
            'agosto' => 'Agosto',
            'setembro' => 'Setembro',
            'outubro' => 'Outubro',
            'novembro' => 'Novembro',
            'dezembro' => 'Dezembro',
        ];
        return $meses[$mes];
    }

    public function getMonthKey($mes){
        $meses = [
            'Janeiro',
            'Fevereiro',
            'Março',
            "Abril",
            'Maio',
            "Junho",
            'Julho',
            'Agosto',
            'Setembro',
            'Outubro',
            'Novembro',
            'Dezembro',
        ];
        return array_search($mes,$meses);
    }

    public function getCategoriaKey($categoria){
        $categorias = [
            'Alimentos',
            'Higiene Pessoal',
            'Limpesa',
        ];
        return array_search($categoria,$categorias);
    }

    public function getCategoryName($categoria){
        $categorias = [
            'alimentos' => 'Alimentos',
            'limpeza' => 'Limpesa',
            'higiene_pessoal' => 'Higiene Pessoal',
        ];
        return $categorias[$categoria];
    }

    public function correctSpelling($text){
        return $this->correctSpelling->execute($text);
    }

    public function getPath(){
        return $this->path;
    }

}