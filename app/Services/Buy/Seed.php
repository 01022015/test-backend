<?php

namespace App\Services\Buy;

use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\BuyRepository;
use App\Buy;
use App\Category;
use App\Product;

class Seed{

    private $read;
    private $buyRepository;
    private $categoryRepository;
    private $productRepository;

    public function __construct(Read $read, BuyRepository $buyRepository,CategoryRepository $categoryRepository, ProductRepository $productRepository)
    {
        $this->read = $read;
        $this->buyRepository = $buyRepository;
        $this->categoryRepository = $categoryRepository;
        $this->productRepository = $productRepository;
    }

    public function execute(){

        $records = $this->read->execute();
        if(!$records) throw new ExceptionEmptyData;

        foreach($records as $data){


            $category = new Category;
            $category->name = $data['categoria'];

            $category = $this->categoryRepository->saveOrUpdate($category);


            $product = new Product;
            $product->name = $data['produto'];
            $product->category_id = $category->id;
            $product = $this->productRepository->saveOrUpdate($product);

            $buy = new Buy;
            $buy->product_id = $product->id;
            $buy->quantity = $data['quantidade'];
            $buy->month = $data['mes'];

            if(!$this->buyRepository->save($buy)) return false;

        }

        return true;

    }

}