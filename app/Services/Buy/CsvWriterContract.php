<?php
namespace App\Services\Buy;

interface CsvWriterContract {
    public function save($header,$records,$path);
}