<?php


namespace App\Services\Buy;

class GenerateCsv{

    private $read;
    private $csvWriter;
    private $path;

    public function __construct(Read $read,CsvWriterContract $csvWriter)
    {
        $this->read = $read;
        $this->csvWriter = $csvWriter;
    }

    public function execute(){

        $data = $this->read->execute();
        if(!$data) throw new ExceptionEmptyData;

        $filename = pathinfo($this->read->getPath())['filename'];

        // $pathCsv = storage_path("app/buy/csv") . "/{$filename}-".time().".csv";
        $pathCsv = storage_path("app/buy/csv") . "/{$filename}.csv";

        $this->path = $pathCsv;

        $headers = ["Mês","Categoria","Produto","Quantidade"];

        if($this->csvWriter->save($headers,$data,$pathCsv)){
            return true;
        }

        return false;

    }

    public function getPath()
    {
        return $this->path;
    }
}